import math

def hitung_error(latitude,longitude,latitude_tujuan,longitude_tujuan,latitude_titik,longitude_titik):
    a = (latitude_tujuan-latitude[0])/(longitude_tujuan-longitude[0])
    b = -1
    c = -a*longitude[0]+latitude[0]
    return (a*longitude_titik+b*latitude_titik+c)/(math.sqrt((a**2)+(b**2)))

# titik_tujuan = [
#     [-0.533489,117.123383],
#     [-0.533525,117.123314],
#     [-0.533614,117.123361]
# ]
titik_tujuan = [
    [-0.5334418333333333,117.12338]
]

latitude_longitude_training = [
[117.12341166666667 , -0.5335333333333333],
[117.12341166666667 , -0.5335331666666666],
[117.12341666666667 , -0.533529],
[117.12341333333333 , -0.5335215000000001],
[117.123405     , -0.5335166666666668],
[117.12339666666666 , -0.5335098333333333],
[117.12339666666666 , -0.5335005],
[117.12339666666666 , -0.5334903333333333],
[117.12339833333333 , -0.5334798333333333],
[117.12339333333334 , -0.5334746666666667],
[117.12338666666666 , -0.5334646666666667],
[117.12339      , -0.5334565],
[117.123385     , -0.5334475],
[117.12338      , -0.5334418333333333]
]

latitude = [] #mendeklarasikan list koordinat latitude sekarang
longitude = [] #mendeklarasikan list koordinat longitude sekarang

waypoint = 0
while waypoint<len(titik_tujuan):
  print("waypoint =",waypoint,"==========================================================================================================================")
  for z in range (len(latitude_longitude_training)):
    latitude.append(latitude_longitude_training[z][1])
    longitude.append(latitude_longitude_training[z][0])

    
    if len(latitude)!=1 :
      latitude_tujuan = titik_tujuan[waypoint][0]
      longitude_tujuan = titik_tujuan[waypoint][1]

      print(latitude)

      print("titik awal = sumbu (",longitude[0],",",latitude[0],")")
      print("titik tujuan = sumbu(",longitude_tujuan,",",latitude_tujuan,")")
      print()
      print("gerak awal = sumbu(",longitude[-2],",",latitude[-2],")")
      print("gerak akhir = sumbu(",longitude[-1],",",latitude[-1],")")
      print()

      try:
        g_vektor_jalur = ((latitude_tujuan-latitude[0])/(longitude_tujuan-longitude[0]))
      except:
        g_vektor_jalur = ("jalur vertikal")
      print("m jalur = ",g_vektor_jalur)

      
      selisih_long = longitude[0]-longitude_tujuan
      selisih_lat = latitude[0]-latitude_tujuan

      if selisih_long<0 and selisih_lat<0 :
        k1 = "arah timur laut"
      elif selisih_long>0 and selisih_lat<0 :
        k1 = "arah barat laut"
      elif selisih_long>0 and selisih_lat>0 :
        k1 = "arah barat daya"
      elif selisih_long<0 and selisih_lat>0 :
        k1 = "arah tenggara"
      elif selisih_long==0 and selisih_lat>0 :
        k1 = "arah selatan"
      elif selisih_long==0 and selisih_lat<0 :
        k1 = "arah utara"
      elif selisih_long>0 and selisih_lat==0:
        k1 = "arah barat"
      elif selisih_long<0 and selisih_lat==0:
        k1 = "arah timur"
      elif selisih_long==0 and selisih_lat==0:
        k1 = "tidak bergerak"
        break
      
      print(k1)

      # print()

      try:
        g_vektor_gerak = ((latitude[-1]-latitude[-2])/(longitude[-1]-longitude[-2]))
      except:
        g_vektor_gerak = ("gerak vertikal")
      print("m gerak = ",g_vektor_gerak)

      selisih_long = longitude[-2]-longitude[-1]
      selisih_lat = latitude[-2]-latitude[-1]

      if selisih_long<0 and selisih_lat<0 :
        k2 = "arah timur laut"
      elif selisih_long>0 and selisih_lat<0 :
        k2 = "arah barat laut"
      elif selisih_long>0 and selisih_lat>0 :
        k2 = "arah barat daya"
      elif selisih_long<0 and selisih_lat>0 :
        k2 = "arah tenggara"
      elif selisih_long==0 and selisih_lat>0 :
        k2 = "arah selatan"
      elif selisih_long==0 and selisih_lat<0 :
        k2 = "arah utara"
      elif selisih_long>0 and selisih_lat==0:
        k2 = "arah barat"
      elif selisih_long<0 and selisih_lat==0:
        k2 = "arah timur"
      elif selisih_long==0 and selisih_lat==0:
        k2 = "tidak bergerak"
        break
      
      print(k2)

      print()

      if g_vektor_jalur!="jalur vertikal" and g_vektor_gerak!="gerak vertikal":
        if g_vektor_jalur*g_vektor_gerak!=-1:
          tan_a = (g_vektor_gerak-g_vektor_jalur)/(1+g_vektor_jalur*g_vektor_gerak)
          sudut = math.degrees(math.atan(tan_a))
        else:
          print("gerak motor tegak lurus")
      else:
        tan_a = (0-g_vektor_jalur)/(1+g_vektor_jalur*0)
        sudut = math.degrees(math.atan(tan_a))
        sudut = -(90-sudut)

      print("ERROR awal =",hitung_error(latitude,longitude,latitude_tujuan,longitude_tujuan,latitude[-2],longitude[-2])*111132.95254792," meter")
      
      print("ERROR akhir =",hitung_error(latitude,longitude,latitude_tujuan,longitude_tujuan,latitude[-1],longitude[-1])*111132.95254792," meter")

      print("Error sudut =",sudut)

      error_tujuan_x = latitude_tujuan-latitude[-1]
      error_tujuan_y = longitude_tujuan-longitude[-1]

      if error_tujuan_x==0 and error_tujuan_y==0:
        waypoint = waypoint + 1
        latitude = []
        longitude =[]
        print("waypoint",waypoint," tercapai")
        break

      # if k1=="arah kuadran 1" or k1=="arah kuadran 2" or k1=="arah horizontal negatif":
      #   if sudut>0:
      #     print("servo belok kanan")
      #   elif sudut<0:
      #     print("servo belok kiri")
      #   elif k1=="arah kuadran 3"

    else :
      print("Mulai Bergerak")
      # print(latitude)

    print("===============================")
