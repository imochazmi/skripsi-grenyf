import math

def hitung_error(latitude,longitude,latitude_tujuan,longitude_tujuan,latitude_titik,longitude_titik):
  
  if longitude_tujuan-longitude[0]==0: # sumbu x tetap / jalur vertikal
    if latitude_tujuan-latitude[0]>0: # utara
      x = 1
    elif latitude_tujuan-latitude[0]<0: #selatan
      x = -1
    else:
      x = 0
    return x*(longitude_titik-longitude[0])

  else:
    if longitude_tujuan-longitude[0]>0: # jalur timur / timur laut / tenggara
      x = 1
      
    elif longitude_tujuan-longitude[0]<0: # jalur barat / barat laut / baratdaya
      x = -1

    a = (latitude_tujuan-latitude[0])/(longitude_tujuan-longitude[0])
    b = -1
    c = -a*longitude[0]+latitude[0]
    d = (a*longitude_titik+b*latitude_titik+c)/(math.sqrt((a**2)+(b**2)))
    return x*d

def hitung_error_jarak(latitude_tujuan, longitude_tujuan, latitude_titik, longitude_titik):
  d = math.fabs(math.sqrt((math.fabs(latitude_tujuan-latitude_titik))**2+(longitude_tujuan-longitude_titik)**2))
  return d

# titik_tujuan = [
#     [-0.533489,117.123383],
#     [-0.533525,117.123314],
#     [-0.533614,117.123361]
# ]
titik_tujuan = [
# lat=y, long=x
    [0,3],
    [3,6],
    [6,6],
    [9,3],
    [9,0],
    [6,-3],
    [3,-3],
    [0,0]
]

latitude_longitude_training = [
# long=x, lat=y
    [0,0],
    [1,-1],
    [2,1],
    [3,0],
    [4.5,0],
    [4.5,3],
    [6,3],
    [7,4],
    [5,5],
    [6,6],
    [6,7.5],
    [3,7.5],
    [3,9],
    [2,10],
    [1,8],
    [0,9],
    [-1.5,9],
    [-1.5,6],
    [-3,6],
    [-4,5],
    [-2,4],
    [-3,3],
    [-3,1.5],
    [0,1.5],
    [0,0]
]

latitude = [] #mendeklarasikan list koordinat latitude sekarang
longitude = [] #mendeklarasikan list koordinat longitude sekarang

waypoint = 0

while waypoint<len(titik_tujuan):

  print("waypoint =",waypoint,"==========================================================================================================================")

  for z in range (len(latitude_longitude_training)-waypoint*3):

    latitude.append(latitude_longitude_training[waypoint*3+z][1])
    longitude.append(latitude_longitude_training[waypoint*3+z][0])
    
    if len(latitude)!=1 :
      latitude_tujuan = titik_tujuan[waypoint][0]
      longitude_tujuan = titik_tujuan[waypoint][1]

      print(longitude)

      print("titik awal = sumbu (",longitude[0],",",latitude[0],")")
      print("titik tujuan = sumbu(",longitude_tujuan,",",latitude_tujuan,")")
      print()

      print("gerak awal = sumbu(",longitude[-2],",",latitude[-2],")")
      print("gerak akhir = sumbu(",longitude[-1],",",latitude[-1],")")
      print()

      try:
        g_vektor_jalur = ((latitude_tujuan-latitude[0])/(longitude_tujuan-longitude[0]))
      except:
        g_vektor_jalur = ("jalur vertikal")
      print("m jalur = ",g_vektor_jalur)
      
      # selisih_long = longitude[0]-longitude_tujuan
      # selisih_lat = latitude[0]-latitude_tujuan

      # if selisih_long<0 and selisih_lat<0 :
      #   k1 = "arah timur laut"
      # elif selisih_long>0 and selisih_lat<0 :
      #   k1 = "arah barat laut"
      # elif selisih_long>0 and selisih_lat>0 :
      #   k1 = "arah barat daya"
      # elif selisih_long<0 and selisih_lat>0 :
      #   k1 = "arah tenggara"
      # elif selisih_long==0 and selisih_lat>0 :
      #   k1 = "arah selatan"
      # elif selisih_long==0 and selisih_lat<0 :
      #   k1 = "arah utara"
      # elif selisih_long>0 and selisih_lat==0:
      #   k1 = "arah barat"
      # elif selisih_long<0 and selisih_lat==0:
      #   k1 = "arah timur"
      # elif selisih_long==0 and selisih_lat==0:
      #   k1 = "tidak bergerak"
      #   break
      
      # print(k1)

      # print()

      try:
        g_vektor_gerak = ((latitude[-1]-latitude[-2])/(longitude[-1]-longitude[-2]))
      except:
        g_vektor_gerak = ("gerak vertikal")
      print("m gerak = ",g_vektor_gerak)

      # selisih_long = longitude[-2]-longitude[-1]
      # selisih_lat = latitude[-2]-latitude[-1]

      # if selisih_long<0 and selisih_lat<0 :
      #   k2 = "arah timur laut"
      # elif selisih_long>0 and selisih_lat<0 :
      #   k2 = "arah barat laut"
      # elif selisih_long>0 and selisih_lat>0 :
      #   k2 = "arah barat daya"
      # elif selisih_long<0 and selisih_lat>0 :
      #   k2 = "arah tenggara"
      # elif selisih_long==0 and selisih_lat>0 :
      #   k2 = "arah selatan"
      # elif selisih_long==0 and selisih_lat<0 :
      #   k2 = "arah utara"
      # elif selisih_long>0 and selisih_lat==0:
      #   k2 = "arah barat"
      # elif selisih_long<0 and selisih_lat==0:
      #   k2 = "arah timur"
      # elif selisih_long==0 and selisih_lat==0:
      #   k2 = "tidak bergerak"
      #   break
      
      # print(k2)

      # print()

      error_awal = hitung_error(latitude,longitude,latitude_tujuan,longitude_tujuan,latitude[-2],longitude[-2])

      error_akhir = hitung_error(latitude,longitude,latitude_tujuan,longitude_tujuan,latitude[-1],longitude[-1])

      print("ERROR awal =",error_awal," meter")

      if math.fabs(error_awal)<0.5:
        print("di dalam jalur")
      else:
        print("di luar jalur")
      
      print("ERROR akhir =",error_akhir," meter")

      if math.fabs(error_akhir)<0.5:
        print("di dalam jalur")
      else:
        print("di luar jalur")





      if g_vektor_jalur=="jalur vertikal" and g_vektor_gerak=="gerak vertikal":
        sudut = 0

      elif g_vektor_jalur!="jalur vertikal" and g_vektor_gerak!="gerak vertikal":

        if g_vektor_jalur*g_vektor_gerak!=-1:
          tan_a = (g_vektor_jalur-g_vektor_gerak)/(1+g_vektor_jalur*g_vektor_gerak)
          sudut = math.degrees(math.atan(tan_a))
        else:
          sudut = 90

      elif g_vektor_jalur=="jalur vertikal":

        if g_vektor_gerak>0:
          tan_a = (0-g_vektor_gerak)/(1+0*g_vektor_gerak)
          sudut = math.degrees(math.atan(tan_a))+90
        elif g_vektor_gerak<0:
          tan_a = (0-g_vektor_gerak)/(1+0*g_vektor_gerak)
          sudut = math.degrees(math.atan(tan_a))-90
        else:
          sudut = 90

      elif g_vektor_gerak=="gerak vertikal":

        if g_vektor_jalur>0:
          tan_a = (g_vektor_jalur-0)/(1+g_vektor_jalur*0)
          sudut = math.degrees(math.atan(tan_a))-90
        elif g_vektor_jalur<0:
          tan_a = (g_vektor_jalur-0)/(1+g_vektor_jalur*0)
          sudut = math.degrees(math.atan(tan_a))+90
        else:
          sudut = 90

      print("Error sudut =",sudut)

      print()

      error_jarak_awal = hitung_error_jarak(latitude_tujuan,longitude_tujuan,latitude[-2],longitude[-2])

      print("Error jarak awal =", error_jarak_awal)

      if error_jarak_awal<0.5:
        print("di dalam waypoint")
      else:
        print("di luar waypoint")

      error_jarak_akhir = hitung_error_jarak(latitude_tujuan,longitude_tujuan,latitude[-1],longitude[-1])

      print("Error jarak akhir =", error_jarak_akhir)

      if error_jarak_akhir<0.5:
        print("di dalam waypoint")
        waypoint = waypoint + 1
        latitude = [latitude_tujuan]
        longitude =[longitude_tujuan]
        print("waypoint",waypoint," tercapai")
        break
      else:
        print("di luar waypoint")
        

      # if k1=="arah kuadran 1" or k1=="arah kuadran 2" or k1=="arah horizontal negatif":
      #   if sudut>0:
      #     print("servo belok kanan")
      #   elif sudut<0:
      #     print("servo belok kiri")
      #   elif k1=="arah kuadran 3"

    else :
      print("Mulai Bergerak")
      # print(latitude)

    print("===============================")
