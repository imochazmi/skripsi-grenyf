import time
import datetime

now = datetime.datetime.now()
print("Start : %s" % time.ctime())
print("sample_data_{}{}{}_{}{}{}".format(now.year,now.month,now.day,now.hour,now.minute,now.second))
time.sleep(5)
now = datetime.datetime.now()
print("End : %s" % time.ctime())
print("sample_data_{}{}{}_{}{}{}".format(now.year,now.month,now.day,now.hour,now.minute,now.second))
