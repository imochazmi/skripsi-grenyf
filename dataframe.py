import pandas as pd
import datetime

list1 = [10,10,30,40]
list2 = [40,30,20,10]

col1 = "No"
col2 = "Latitude (derajat)"
col3 = "Longitude (derajat)"
col4 = "Error Jalur"
col5 = "Error Jarak"
col6 = "Error Sudut"
col7 = "Keterangan"
data = pd.DataFrame({col1:list1,col2:list2})

now = datetime.datetime.now()
data.to_excel('sample_data_{}{}{}_{}{}{}.xlsx'.format(now.year,now.month,now.day,now.hour,now.minute,now.second),sheet_name='sheet1', index=False)
print("Berhasil")