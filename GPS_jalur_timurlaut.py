import math

def hitung_error(latitude,longitude,latitude_tujuan,longitude_tujuan,latitude_titik,longitude_titik):
    a = (latitude_tujuan-latitude[0])/(longitude_tujuan-longitude[0])
    b = -1
    c = -a*longitude[0]+latitude[0]
    return (a*longitude_titik+b*latitude_titik+c)/(math.sqrt((a**2)+(b**2)))

# titik_tujuan = [
#     [-0.533489,117.123383],
#     [-0.533525,117.123314],
#     [-0.533614,117.123361]
# ]
titik_tujuan = [
    [-0.5334396874134,117.1234464024359]
]


latitude_training = [
-0.5335333333333333,
-0.53352187359,
-0.5335103814552,
-0.5335013270459,
-0.5334933173761,
-0.533489834911,
-0.53348,
-0.5334661541482,
-0.533457796232,
-0.5334480453296,
-0.5334463040971,
-0.5334396874134
]

longitude_training = [
117.12341166666667,
117.1234227216731,
117.1234171497289,
117.1234154084964,
117.1234209804406,
117.1234317760824,
117.12344,
117.123432820822,
117.1234317760824,
117.1234390892592,
117.1234495366545,
117.1234464024359
]

latitude = [] #mendeklarasikan list koordinat latitude sekarang
longitude = [] #mendeklarasikan list koordinat longitude sekarang

waypoint = 0
while waypoint<len(titik_tujuan):
  print("waypoint =",waypoint,"==========================================================================================================================")
  for z in range (len(latitude_training)):
    latitude.append(latitude_training[z])
    longitude.append(longitude_training[z])

    
    if len(latitude)!=1 :
      latitude_tujuan = titik_tujuan[waypoint][0]
      longitude_tujuan = titik_tujuan[waypoint][1]

      print(latitude)

      print("titik awal = sumbu (",longitude[0],",",latitude[0],")")
      print("titik tujuan = sumbu(",longitude_tujuan,",",latitude_tujuan,")")
      print()
      print("gerak awal = sumbu(",longitude[-2],",",latitude[-2],")")
      print("gerak akhir = sumbu(",longitude[-1],",",latitude[-1],")")
      print()

      try:
        g_vektor_jalur = ((latitude_tujuan-latitude[0])/(longitude_tujuan-longitude[0]))
      except:
        g_vektor_jalur = ("jalur vertikal")
      print("m jalur = ",g_vektor_jalur)

      
      selisih_long = longitude[0]-longitude_tujuan
      selisih_lat = latitude[0]-latitude_tujuan

      if selisih_long<0 and selisih_lat<0 :
        k1 = "arah timur laut"
      elif selisih_long>0 and selisih_lat<0 :
        k1 = "arah barat laut"
      elif selisih_long>0 and selisih_lat>0 :
        k1 = "arah barat daya"
      elif selisih_long<0 and selisih_lat>0 :
        k1 = "arah tenggara"
      elif selisih_long==0 and selisih_lat>0 :
        k1 = "arah selatan"
      elif selisih_long==0 and selisih_lat<0 :
        k1 = "arah utara"
      elif selisih_long>0 and selisih_lat==0:
        k1 = "arah barat"
      elif selisih_long<0 and selisih_lat==0:
        k1 = "arah timur"
      elif selisih_long==0 and selisih_lat==0:
        k1 = "tidak bergerak"
        break
      
      print(k1)

      # print()

      try:
        g_vektor_gerak = ((latitude[-1]-latitude[-2])/(longitude[-1]-longitude[-2]))
      except:
        g_vektor_gerak = ("gerak vertikal")
      print("m gerak = ",g_vektor_gerak)

      selisih_long = longitude[-2]-longitude[-1]
      selisih_lat = latitude[-2]-latitude[-1]

      if selisih_long<0 and selisih_lat<0 :
        k2 = "arah timur laut"
      elif selisih_long>0 and selisih_lat<0 :
        k2 = "arah barat laut"
      elif selisih_long>0 and selisih_lat>0 :
        k2 = "arah barat daya"
      elif selisih_long<0 and selisih_lat>0 :
        k2 = "arah tenggara"
      elif selisih_long==0 and selisih_lat>0 :
        k2 = "arah selatan"
      elif selisih_long==0 and selisih_lat<0 :
        k2 = "arah utara"
      elif selisih_long>0 and selisih_lat==0:
        k2 = "arah barat"
      elif selisih_long<0 and selisih_lat==0:
        k2 = "arah timur"
      elif selisih_long==0 and selisih_lat==0:
        k2 = "tidak bergerak"
        break
      
      print(k2)

      print()

      if g_vektor_jalur!="jalur vertikal" and g_vektor_gerak!="gerak vertikal":
        if g_vektor_jalur*g_vektor_gerak!=-1:
          tan_a = (g_vektor_gerak-g_vektor_jalur)/(1+g_vektor_jalur*g_vektor_gerak)
          sudut = math.degrees(math.atan(tan_a))
        else:
          print("gerak motor tegak lurus")
      else:
        tan_a = (0-g_vektor_jalur)/(1+g_vektor_jalur*0)
        sudut = math.degrees(math.atan(tan_a))
        sudut = -(90-sudut)

      print("ERROR awal =",hitung_error(latitude,longitude,latitude_tujuan,longitude_tujuan,latitude[-2],longitude[-2])*111132.95254792," meter")
      
      print("ERROR akhir =",hitung_error(latitude,longitude,latitude_tujuan,longitude_tujuan,latitude[-1],longitude[-1])*111132.95254792," meter")

      print("Error sudut =",sudut)

      error_tujuan_x = latitude_tujuan-latitude[-1]
      error_tujuan_y = longitude_tujuan-longitude[-1]

      if error_tujuan_x==0 and error_tujuan_y==0:
        waypoint = waypoint + 1
        latitude = []
        longitude =[]
        print("waypoint",waypoint," tercapai")
        break

      # if k1=="arah kuadran 1" or k1=="arah kuadran 2" or k1=="arah horizontal negatif":
      #   if sudut>0:
      #     print("servo belok kanan")
      #   elif sudut<0:
      #     print("servo belok kiri")
      #   elif k1=="arah kuadran 3"

    else :
      print("Mulai Bergerak")
      # print(latitude)

    print("===============================")
