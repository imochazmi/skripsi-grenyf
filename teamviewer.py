import RPi.GPIO as GPIO
from time import sleep #impor library untuk mengatur nilai tunda
import serial #impor library untuk komunikasi serial
import datetime #impor datetime untuk menggunakan tanggal dan waktu
import math #impor library untuk nilai matematika
import time #impor library waktu

GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.OUT)
GPIO.setwarnings(False)
GPIO.setup(12, GPIO.OUT)

def dec2deg(value): #mendeklarasikan nilai dalam suatu fungsi
    dec = value/100.00 #perhitungan nilai menjadi desimal
    deg = int(dec) #membuat angka desimal menjadi bilangan bulat
    min = (dec - int(dec))/0.6 #perhitungan nilai min
    position = dec + min #perhitungan posisi
    position = "%.7f" %position #perhitungan posisi
    return position #mengembalikan nilai posisi

def setAngle(angle): #mendeklarasikan nilai sudut
    duty = (angle / 18) + 2 #perhitungan nilai duty
    GPIO.output(11, True)
    pwm.ChangeDutyCycle(duty)
    GPIO.output(11, False)
    pwm.ChangeDutyCycle(duty)
    
def hitung_error(latitude,longitude,latitude_tujuan,longitude_tujuan,latitude_titik,longitude_titik):
    a = (longitude_tujuan-longitude[0])/(latitude_tujuan-latitude[0])
    b = -1
    c = -a*latitude[0]+longitude[0]
    return (a*latitude_titik+b*longitude_titik+c)/(math.sqrt((a**2)+(b**2)))
    
mapscale = 18 #mengatur nilai mapscale
latitude = [] #mendeklarasikan list koordinat latitude sekarang
longitude = [] #mendeklarasikan list koordinat longitude sekarang

f = GPIO.PWM(12,100) #inisialisasi pin PWM
f.start(0) 
f.ChangeDutyCycle(90) #pengaturan nilai PWM

pwm = GPIO.PWM(11, 50) #inisialisasi pin PWM
pwm.start(0)
waypoint = 0

# list_waypoint = [1, 2, 3]
# list_latitude = [ -0.533489, -0.533525, -0.533614]
# list_longitude = [117.123383, 117.123314, 117.123361]
titik_tujuan = [
    [-0.533489,117.123383],
    [-0.533525,117.123314],
    [-0.533614,117.123361]
]
i = 0 
    
while waypoint<len(titik_tujuan): #mengulang suatu blok kode program ketika pernyataan berlogika True
    port="/dev/serial0" #mendeklarasikan port serial UART
    ser=serial.Serial(port, baudrate=9600, timeout=0.5) #mengakses serial port dengan mengatur nilai baudrate dan timeout
    gpsdata=ser.readline() #mengembalikan list yang berisi baris – baris file dari awal sampai akhir pada variabel gpsdata
    try: #menguji suatu pernyataan
        # print(gpsdata)
        gpsdata = gpsdata.decode("utf8") #proses komunikasi dengan memasukkan utf8 ke dalam variabel gpsdata
        try: #menguji suatu pernyataan
            # print(gpsdata)
            gpsdata = gpsdata.split(',') #memisahkan antar data pada variabel gpsdata dengan koma
            # print(gpsdata)
            if "GNRMC" in gpsdata[0]: #mengeksekusi kode jika kondisi True dan sebaliknya
                hrs, min, sec = gpsdata[1][0:2], gpsdata[1][2:4]. gpsdata[1][4:6] #memasukkan nilai gpsdata ke dalam format variabel hrs, min, secssssss
                day, month, year = gpsdata[9][0:2], gpsdata[9][2:4], gpsdata[9][4:6] #memasukkan indeks tertentu dari gpsdata ke dalam variabel day, month, year
                datetimeutc = "{}:{}:{} {}/{}/{}".format(hrs, min, sec, day, month, year) #memasukkan nilai jam, menit, detik, hari, bulan, tahun ke dalam format variabel datetimeutc
                datetimeutc = datetime.datetime.strptime(datetimeutc, '%H:%M:%S %d/%m/%y') #memasukkan nilai datetimeutc ke dalam variabel datetimeutc
                speed = round(float(gpsdata[7])*1.852,2) #perhitungan kecepatan
                message = "Datetime={} ,speed={} kmph".format(datetimeutc, speed) #memasukkan nilai datetimeutc dan speed ke dalam variabel message
                print(message) #menampilkan message
            if "GNGGA" in gpsdata[0]: #mengeksekusi kode jika kondisi True dan sebaliknya
                # print(gpsdata)
                lat = dec2deg(float(gpsdata[2])) #memasukkan indeks ke 2 dari gps data ke dalam variabel lat dan mengonversinya ke dalam decimal degree
                lon = dec2deg(float(gpsdata[4])) #memasukkan indeks ke 2 dari gps data ke dalam variabel lon dan mengonversinya ke dalam decimal degree
                alt = gpsdata[9] #memasukkan indeks ke 9 dari gpsdata ke dalam variabel alt
                satcount = gpsdata[7] #memasukkan indeks ke 7 dari gpsdata ke dalam variabel satcount
                message = "Altitude={}, Satellites={}\n".format(alt, satcount) #memasukkan nilai alt dan satcount pada format dalam format variabel message
                gearth = "https://earth.google.com/web/search/@{},{},{}\n".format(lat,lon,alt) #memasukkan nilai lat, lon, dan alt ke dalam format link gearth
                mapsapp = "geo:{},{}\n".format(lat, lon) #memasukkan nilai lat dan lon ke dalam format variabel mapsapp
                map = "https://www.openstreetmap.org/#map={}/{}/{}\n\n".format(mapscale, lat, lon) #memasukkan nilai mapscale, lat, dan lon ke dalam link variabel map
                print(message, gearth, mapsapp, map) #menampilkan message, gearth, mapsapp, dan map
                
        except: # menguji suatu pernyataan
            # print(gpsdata)
            data_latitude = float(gpsdata[3]) #memasukkan indeks ke 3 dari gpsdata ke variabel latitude
            data_longitude = float(gpsdata[5]) #memasukkan indeks ke 5 dari gpsdata ke variabel longitude

            if gpsdata[4]=="N" :
        dim_lat = 1
      elif gpsdata[4]=="S":
        dim_lat = -1

      xlat = float(dim_lat*((round(data_latitude,-2))+((data_latitude - round(data_latitude,-2))/60))) #perhitungan nilai xlat
      print(xlat)

      if gpsdata[6]=="E" :
        dim_long = 1
      elif gpsdata[6]=="W":
        dim_long = -1

      ylong = float(dim_long*((round(data_longitude,-2)) + ((data_longitude - round(data_longitude,-2))/60))) #perhitungan nilai ylong
      print(ylong)

            time = str(gpsdata[1]) #memasukkan indeks ke 1 dari gpsdata ke variabel waktu
            date = gpsdata[9] #memasukkan indeks ke 9 dari gpsdata ke variabel tanggal

            # print("Latitude = ", xlat,'derajat\nLongitude  = ', ylong,'derajat') #mencetak angka koordinat pada variabel xlat dan ylong ke dalam bentuk decimal degree
            
            latitude.append(xlat) #memasukkan nilai pada variabel xlat ke dalam list latitude sekarang
            longitude.append(ylong) #memasukkan nilai pada variabel xlat ke dalam list longitude sekaran

            if len(latitude)!=1 :
              latitude_tujuan = titik_tujuan[waypoint][0]
              longitude_tujuan = titik_tujuan[waypoint][1]

              print("titik awal = (",latitude[0],",",longitude[0],")")
              print("titik tujuan = (",latitude_tujuan,",",longitude_tujuan,")")
              print()
              print("gerak awal = (",latitude[-2],",",longitude[-2],")")
              print("gerak akhir = (",latitude[-1],",",longitude[-1],")")
              print()

              error_tujuan_x = latitude_tujuan-latitude[-2]
              error_tujuan_y = longitude_tujuan-longitude[-2]

              if error_tujuan_x==0 and error_tujuan_y==0:
                waypoint = waypoint + 1
                latitude = []
                longitude =[]
                print("waypoint",waypoint," tercapai")
                break

              try:
                g_vektor_jalur = ((longitude_tujuan-longitude[0])/(latitude_tujuan-latitude[0]))
                print("m jalur = ",g_vektor_jalur)
              except:
                g_vektor_jalur = ("jalur vertikal")

              selisih_lat = latitude[0]-latitude_tujuan
              selisih_long = longitude[0]-longitude_tujuan

              if selisih_lat<0 and selisih_long<0:
                k1 = "arah kuadran 1"
              elif selisih_lat<0 and selisih_long>0:
                k1 = "arah kuadran 2"
              elif selisih_lat>0 and selisih_long>0:
                k1 = "arah kuadran 3"
              elif selisih_lat>0 and selisih_long<0:
                k1 = "arah kuadran 4"
              elif selisih_lat>0 and selisih_long==0:
                k1 = "horizontal negatif"
              elif selisih_lat<0 and selisih_long==0:
                k1 = "horizontal positif"
              elif selisih_lat==0 and selisih_long>0:
                k1 = "vertikal negatif"
              elif selisih_lat==0 and selisih_long<0:
                k1 = "vertikal positif"
              elif selisih_lat==0 and selisih_long==0:
                k1 = "berhenti"
                break
              
              print(k1)

              print()

              try:
                g_vektor_gerak = ((longitude[-1]-longitude[-2])/(latitude[-1]-latitude[-2]))
              except:
                g_vektor_gerak = ("gerak vertikal")
              print("m gerak = ",g_vektor_gerak)

              selisih_lat = latitude[-2]-latitude[-1]
              selisih_long = longitude[-2]-longitude[-1]

              if selisih_lat<0 and selisih_long<0:
                k2 = "arah kuadran 1"
              elif selisih_lat<0 and selisih_long>0:
                k2 = "arah kuadran 2"
              elif selisih_lat>0 and selisih_long>0:
                k2 = "arah kuadran 3"
              elif selisih_lat>0 and selisih_long<0:
                k2 = "arah kuadran 4"
              elif selisih_lat>0 and selisih_long==0:
                k2 = "horizontal negatif"
              elif selisih_lat<0 and selisih_long==0:
                k2 = "horizontal positif"
              elif selisih_lat==0 and selisih_long>0:
                k2 = "vertikal negatif"
              elif selisih_lat==0 and selisih_long<0:
                k2 = "vertikal positif"
              elif selisih_lat==0 and selisih_long==0:
                k2 = "berhenti"
                break
              
              print(k2)

              print()

              if g_vektor_jalur!="jalur vertikal" and g_vektor_gerak!="gerak vertikal":
                if g_vektor_jalur*g_vektor_gerak!=-1:
                  tan_a = (g_vektor_gerak-g_vektor_jalur)/(1+g_vektor_jalur*g_vektor_gerak)
                  sudut = math.degrees(math.atan(tan_a))
                else:
                  print("gerak motor tegak lurus")
              else:
                tan_a = (0-g_vektor_jalur)/(1+g_vektor_jalur*0)
                sudut = math.degrees(math.atan(tan_a))
                sudut = -(90-sudut)

              print("ERROR awal =",hitung_error(latitude,longitude,latitude_tujuan,longitude_tujuan,latitude[-2],longitude[-2]))
              
              print("ERROR akhir =",hitung_error(latitude,longitude,latitude_tujuan,longitude_tujuan,latitude[-1],longitude[-1]))

              print("Error sudut",sudut)

              # if k1=="arah kuadran 1" or k1=="arah kuadran 2" or k1=="arah horizontal negatif":
              #   if sudut>0:
              #     print("servo belok kanan")
              #   elif sudut<0:
              #     print("servo belok kiri")

              # if k1=="arah kuadran 3"

            else :
              print("data tunggal")

            print("===============================")        
            
    except:
        print("in progress")

pwm.stop()
GPIO.cleanup()