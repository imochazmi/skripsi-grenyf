pi@raspberrypi:~/Documents/FIX PROGRAM SKRIPSI $ /bin/python3 "/home/pi/Documents/FIX PROGRAM SKRIPSI/PROGRAM_GPS_AUTONOMOUS_CAR.py"
Keluar

Masuk
data ke 1

mulai bergerak

Waypoint tercapai =  []

===============================
Masuk
data ke 2

titik awal =  ( 117.1234395 , -0.5335711666666667 )
titik tujuan = ( 117.123378 , -0.533475 )

Longitude latitude terukur sekarang = ( 117.12343983333334 , -0.5335716666666667 )

Error jalur = 1.1437167783560855e-08  derajat
Error sudut = 0 derajat

Long Lat seharusnya = ( 117.12343982369799 , -0.5335716728286106 )

lurus

Waypoint tercapai =  []

===============================
Masuk
data ke 3

titik awal =  ( 117.1234395 , -0.5335711666666667 )
titik tujuan = ( 117.123378 , -0.533475 )

Longitude latitude terukur sekarang = ( 117.1234385 , -0.5335701666666667 )

Error jalur = -3.036933228686322e-07  derajat
Error sudut = -90 derajat

Long Lat seharusnya = ( 117.12343875584855 , -0.5335700030477515 )

belok kanan

Waypoint tercapai =  []

===============================
Masuk
data ke 4

titik awal =  ( 117.1234395 , -0.5335711666666667 )
titik tujuan = ( 117.123378 , -0.533475 )

Longitude latitude terukur sekarang = ( 117.12343183333333 , -0.533562 )

Error jalur = -1.5201700320849148e-06  derajat
Error sudut = -6.626160054702864 derajat

Long Lat seharusnya = ( 117.12343311401118 , -0.5335611809876487 )

lurus

Waypoint tercapai =  []

===============================
Masuk
data ke 5

titik awal =  ( 117.1234395 , -0.5335711666666667 )
titik tujuan = ( 117.123378 , -0.533475 )

Longitude latitude terukur sekarang = ( 117.12342983333335 , -0.5335515 )

Error jalur = 2.451934269295548e-06  derajat
Error sudut = 90 derajat

Long Lat seharusnya = ( 117.12342776768423 , -0.5335528210130377 )

belok kiri

Waypoint tercapai =  []

===============================
Masuk
data ke 6

titik awal =  ( 117.1234395 , -0.5335711666666667 )
titik tujuan = ( 117.123378 , -0.533475 )

Longitude latitude terukur sekarang = ( 117.12342733333335 , -0.5335396666666666 )

Error jalur = 6.721161600781986e-06  derajat
Error sudut = 20.670191943328682 derajat

Long Lat seharusnya = ( 117.12342167104396 , -0.5335432877841491 )

belok kiri

Waypoint tercapai =  []

===============================
Masuk
data ke 7

titik awal =  ( 117.1234395 , -0.5335711666666667 )
titik tujuan = ( 117.123378 , -0.533475 )

Longitude latitude terukur sekarang = ( 117.12341650000002 , -0.5335331666666666 )

Error jalur = 1.096508128934781e-06  derajat
Error sudut = -26.436729379021934 derajat

Long Lat seharusnya = ( 117.12341557623908 , -0.5335337574253936 )

lurus

Waypoint tercapai =  []

===============================
Masuk
data ke 8

titik awal =  ( 117.1234395 , -0.5335711666666667 )
titik tujuan = ( 117.123378 , -0.533475 )

Longitude latitude terukur sekarang = ( 117.123405 , -0.5335285 )

Error jalur = -6.07751667106051e-06  derajat
Error sudut = -90 derajat

Long Lat seharusnya = ( 117.12341012004626 , -0.5335252256549954 )

belok kanan

Waypoint tercapai =  []

===============================
Masuk
data ke 9

titik awal =  ( 117.1234395 , -0.5335711666666667 )
titik tujuan = ( 117.123378 , -0.533475 )

Longitude latitude terukur sekarang = ( 117.1233965 , -0.5335193333333332 )

Error jalur = -8.299734194368077e-06  derajat
Error sudut = -10.239406443659275 derajat

Long Lat seharusnya = ( 117.12340349216889 , -0.5335148617383226 )

belok kanan

Waypoint tercapai =  []

===============================
Masuk
data ke 10

titik awal =  ( 117.1234395 , -0.5335711666666667 )
titik tujuan = ( 117.123378 , -0.533475 )

Longitude latitude terukur sekarang = ( 117.12339716666665 , -0.5335116666666667 )

Error jalur = -3.6075749871564944e-06  derajat
Error sudut = 37.56925470163874 derajat

Long Lat seharusnya = ( 117.12340020589329 , -0.5335097230364578 )

belok kiri

Waypoint tercapai =  []

===============================
Masuk
data ke 11

titik awal =  ( 117.1234395 , -0.5335711666666667 )
titik tujuan = ( 117.123378 , -0.533475 )

Longitude latitude terukur sekarang = ( 117.12339750000001 , -0.533501 )

Error jalur = 2.420056195799025e-06  derajat
Error sudut = 90 derajat

Long Lat seharusnya = ( 117.12339546120681 , -0.5335023038382846 )

belok kiri

Waypoint tercapai =  []

===============================
Masuk
data ke 12

titik awal =  ( 117.1234395 , -0.5335711666666667 )
titik tujuan = ( 117.123378 , -0.533475 )

Longitude latitude terukur sekarang = ( 117.12338900000002 , -0.5334946666666667 )

Error jalur = -1.3286582741517552e-06  derajat
Error sudut = -90 derajat

Long Lat seharusnya = ( 117.12339011933746 , -0.5334939508338841 )

belok kanan

Waypoint tercapai =  []

===============================
Masuk
data ke 13

titik awal =  ( 117.1234395 , -0.5335711666666667 )
titik tujuan = ( 117.123378 , -0.533475 )

Longitude latitude terukur sekarang = ( 117.12337716666667 , -0.5334886666666667 )

Error jalur = -8.065150558046607e-06  derajat
Error sudut = -30.513551741304084 derajat

Long Lat seharusnya = ( 117.12338396120893 , -0.5334843214567994 )

belok kanan

Waypoint tercapai =  []

===============================
Masuk
data ke 14

titik awal =  ( 117.1234395 , -0.5335711666666667 )
titik tujuan = ( 117.123378 , -0.533475 )

Longitude latitude terukur sekarang = ( 117.1233755 , -0.5334773333333334 )

Error jalur = -3.3632575854806355e-06  derajat
Error sudut = 24.23362791781206 derajat

Long Lat seharusnya = ( 117.12337833339976 , -0.5334755213324457 )
waypoint 1  tercapai

lurus

Waypoint tercapai =  [1]

===============================
Masuk
data ke 15

titik awal =  ( 117.123378 , -0.533475 )
titik tujuan = ( 117.123306 , -0.533511 )

Longitude latitude terukur sekarang = ( 117.12337466666668 , -0.5334681666666667 )

Error jalur = 7.602631123435998e-06  derajat
Error sudut = 0 derajat

Long Lat seharusnya = ( 117.12337806666667 , -0.5334749666666613 )

belok kiri

Waypoint tercapai =  [1]

===============================
Masuk
data ke 16

titik awal =  ( 117.123378 , -0.533475 )
titik tujuan = ( 117.123306 , -0.533511 )

Longitude latitude terukur sekarang = ( 117.12337116666667 , -0.5334583333333334 )

Error jalur = 1.7963079418538017e-05  derajat
Error sudut = 96.97276925732186 derajat

Long Lat seharusnya = ( 117.12337920000002 , -0.5334744000000227 )

belok kiri

Waypoint tercapai =  [1]

===============================
Masuk
data ke 17

titik awal =  ( 117.123378 , -0.533475 )
titik tujuan = ( 117.123306 , -0.533511 )

Longitude latitude terukur sekarang = ( 117.12336250000001 , -0.5334559999999999 )

Error jalur = 2.3925927356244776e-05  derajat
Error sudut = 41.63353934895219 derajat

Long Lat seharusnya = ( 117.12337320000002 , -0.5334774000000093 )

belok kiri

Waypoint tercapai =  [1]

===============================
Masuk
data ke 18

titik awal =  ( 117.123378 , -0.533475 )
titik tujuan = ( 117.123306 , -0.533511 )

Longitude latitude terukur sekarang = ( 117.12335466666667 , -0.5334638333333334 )

Error jalur = 2.0422754193954232e-05  derajat
Error sudut = -18.43494879264734 derajat

Long Lat seharusnya = ( 117.12336380000002 , -0.5334821000000343 )

belok kiri

Waypoint tercapai =  [1]

===============================
Masuk
data ke 19

titik awal =  ( 117.123378 , -0.533475 )
titik tujuan = ( 117.123306 , -0.533511 )

Longitude latitude terukur sekarang = ( 117.12334933333335 , -0.5334715 )

Error jalur = 1.5950618237496517e-05  derajat
Error sudut = -28.610459734633686 derajat

Long Lat seharusnya = ( 117.12335646666668 , -0.533485766666654 )

belok kiri

Waypoint tercapai =  [1]

===============================
Masuk
data ke 20

titik awal =  ( 117.123378 , -0.533475 )
titik tujuan = ( 117.123306 , -0.533511 )

Longitude latitude terukur sekarang = ( 117.12334700000001 , -0.5334791666666666 )

Error jalur = 1.0136841497914664e-05  derajat
Error sudut = -46.50743570277862 derajat

Long Lat seharusnya = ( 117.12335153333336 , -0.5334882333333734 )

lurus

Waypoint tercapai =  [1]

===============================
Masuk
data ke 21

titik awal =  ( 117.123378 , -0.533475 )
titik tujuan = ( 117.123306 , -0.533511 )

Longitude latitude terukur sekarang = ( 117.12334133333334 , -0.5334876666666666 )

Error jalur = 5.068420748957332e-06  derajat
Error sudut = -29.744881298277125 derajat

Long Lat seharusnya = ( 117.12334360000003 , -0.5334922000000342 )

belok kiri

Waypoint tercapai =  [1]

===============================
Masuk
data ke 22

titik awal =  ( 117.123378 , -0.533475 )
titik tujuan = ( 117.123306 , -0.533511 )

Longitude latitude terukur sekarang = ( 117.123333 , -0.5334906666666667 )

Error jalur = 6.111919142186951e-06  derajat
Error sudut = 6.766174837214047 derajat

Long Lat seharusnya = ( 117.12333573333333 , -0.5334961333333311 )

belok kiri

Waypoint tercapai =  [1]

===============================
Masuk
data ke 23

titik awal =  ( 117.123378 , -0.533475 )
titik tujuan = ( 117.123306 , -0.533511 )

Longitude latitude terukur sekarang = ( 117.12332616666666 , -0.5334958333333333 )

Error jalur = 4.546671561875453e-06  derajat
Error sudut = -10.527786075606683 derajat

Long Lat seharusnya = ( 117.12332819999997 , -0.5334998999999672 )

lurus

Waypoint tercapai =  [1]

===============================
Masuk
data ke 24

titik awal =  ( 117.123378 , -0.533475 )
titik tujuan = ( 117.123306 , -0.533511 )

Longitude latitude terukur sekarang = ( 117.1233175 , -0.5334993333333333 )

Error jalur = 5.292027552499976e-06  derajat
Error sudut = 4.573921241047662 derajat

Long Lat seharusnya = ( 117.12331986666666 , -0.5335040666666606 )

belok kiri

Waypoint tercapai =  [1]

===============================
Masuk
data ke 25

titik awal =  ( 117.123378 , -0.533475 )
titik tujuan = ( 117.123306 , -0.533511 )

Longitude latitude terukur sekarang = ( 117.12330616666668 , -0.5335023333333333 )

Error jalur = 7.677166716143162e-06  derajat
Error sudut = 11.738571187837378 derajat

Long Lat seharusnya = ( 117.12330960000001 , -0.5335092 )
waypoint 2  tercapai

belok kiri

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 26

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.12329733333333 , -0.5335076666666667 )

Error jalur = 5.906873678839672e-06  derajat
Error sudut = 0 derajat

Long Lat seharusnya = ( 117.12330247652476 , -0.5335047617159515 )

belok kiri

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 27

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.12329216666667 , -0.5335141666666666 )

Error jalur = 1.36022024914359e-05  derajat
Error sudut = 67.93860296586291 derajat

Long Lat seharusnya = ( 117.12330401028059 , -0.533507477218064 )

belok kiri

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 28

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.12329116666666 , -0.5335215000000001 )

Error jalur = 1.8079387010106427e-05  derajat
Error sudut = 37.22357089551417 derajat

Long Lat seharusnya = ( 117.12330690862314 , -0.5335126087097689 )

belok kiri

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 29

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.12329916666667 , -0.5335273333333334 )

Error jalur = 1.398246756947601e-05  derajat
Error sudut = -24.44331127367633 derajat

Long Lat seharusnya = ( 117.1233113413823 , -0.533520456873576 )

belok kiri

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 30

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.123308 , -0.5335306666666667 )

Error jalur = 7.9304750753562e-06  derajat
Error sudut = -39.86717043407166 derajat

Long Lat seharusnya = ( 117.12331490516739 , -0.5335267665258224 )

belok kiri

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 31

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.12332149999999 , -0.5335256666666666 )

Error jalur = -6.283107657322954e-06  derajat
Error sudut = -90 derajat

Long Lat seharusnya = ( 117.12331602921678 , -0.533528756646071 )

belok kanan

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 32

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.1233295 , -0.5335271666666667 )

Error jalur = -1.2511123932868045e-05  derajat
Error sudut = -49.921939946235995 derajat

Long Lat seharusnya = ( 117.12331860640234 , -0.5335333195320127 )

belok kanan

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 33

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.12332566666667 , -0.5335356666666667 )

Error jalur = -4.993162564275888e-06  derajat
Error sudut = 53.73284586762525 derajat

Long Lat seharusnya = ( 117.12332131905538 , -0.5335381222619353 )

belok kiri

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 34

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.12332183333335 , -0.533546 )

Error jalur = 3.426416690097358e-06  derajat
Error sudut = 90 derajat

Long Lat seharusnya = ( 117.12332481675878 , -0.533544314917117 )

belok kiri

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 35

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.12332966666666 , -0.5335526666666667 )

Error jalur = -1.1555758160157197e-07  derajat
Error sudut = -90 derajat

Long Lat seharusnya = ( 117.12332956604918 , -0.5335527234969123 )

belok kanan

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 36

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.12333666666667 , -0.5335603333333334 )

Error jalur = -2.440146188859841e-06  derajat
Error sudut = -12.939033053443113 derajat

Long Lat seharusnya = ( 117.1233345419998 , -0.5335615333766627 )

lurus

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 37

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.12333599999998 , -0.5335693333333332 )

Error jalur = 2.5664533226877617e-06  derajat
Error sudut = 90 derajat

Long Lat seharusnya = ( 117.12333823464415 , -0.5335680711731995 )

belok kiri

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 38

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.1233375 , -0.5335786666666666 )

Error jalur = 5.8504385645952326e-06  derajat
Error sudut = 20.328228157976255 derajat

Long Lat seharusnya = ( 117.12334259405266 , -0.5335757894702604 )

belok kiri

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 39

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.12334433333332 , -0.5335838333333334 )

Error jalur = 2.441489879393745e-06  derajat
Error sudut = -23.4487578454735 derajat

Long Lat seharusnya = ( 117.12334645917022 , -0.5335826326291582 )

lurus

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 40

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.1233525 , -0.5335893333333334 )

Error jalur = -1.964478915181838e-06  derajat
Error sudut = -90 derajat

Long Lat seharusnya = ( 117.12335078950277 , -0.5335902994475097 )

belok kanan

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 41

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.123356 , -0.5335965 )

Error jalur = -1.4874679649474883e-06  derajat
Error sudut = 3.4288125613978315 derajat

Long Lat seharusnya = ( 117.12335470484238 , -0.5335972315242095 )

lurus

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 42

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.12335300000001 , -0.5336035 )

Error jalur = 4.567211910240131e-06  derajat
Error sudut = 90 derajat

Long Lat seharusnya = ( 117.1233569767306 , -0.5336012538836445 )

belok kiri

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 43

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.12335383333333 , -0.5336105 )

Error jalur = 7.2841588382987625e-06  derajat
Error sudut = 22.669430344808383 derajat

Long Lat seharusnya = ( 117.12336017574479 , -0.5336069177120416 )

belok kiri

Waypoint tercapai =  [1, 2]

===============================
Masuk
data ke 44

titik awal =  ( 117.123306 , -0.533511 )
titik tujuan = ( 117.123367 , -0.533619 )

Longitude latitude terukur sekarang = ( 117.1233605 , -0.533613 )

Error jalur = 2.7088847429225344e-06  derajat
Error sudut = -39.98555002544657 derajat

Long Lat seharusnya = ( 117.12336285866105 , -0.533611667793296 )
waypoint 3  tercapai

belok kanan

Waypoint tercapai =  [1, 2, 3]

===============================
pi@raspberrypi:~/Documents/FIX PROGRAM SKRIPSI $ 