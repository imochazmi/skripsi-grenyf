import RPi.GPIO as GPIO
from time import sleep #impor library untuk mengatur nilai tunda
import serial #impor library untuk komunikasi serial
import datetime #impor datetime untuk menggunakan tanggal dan waktu
import math #impor library untuk nilai matematika
import time #impor library waktu

GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.OUT)
GPIO.setwarnings(False)
GPIO.setup(12, GPIO.OUT)

def dec2deg(value): #mendeklarasikan nilai dalam suatu fungsi
  dec = value/100.00 #perhitungan nilai menjadi desimal
  deg = int(dec) #membuat angka desimal menjadi bilangan bulat
  min = (dec - int(dec))/0.6 #perhitungan nilai min
  position = dec + min #perhitungan posisi
  position = "%.7f" %position #perhitungan posisi
  return position #mengembalikan nilai posisi

def setAngle(angle): #mendeklarasikan nilai sudut
  duty = (angle / 18) + 2 #perhitungan nilai duty
  GPIO.output(11, True)
  pwm.ChangeDutyCycle(duty)
  GPIO.output(11, False)
  pwm.ChangeDutyCycle(duty)
    
def hitung_error(latitude,longitude,latitude_tujuan,longitude_tujuan,latitude_titik,longitude_titik):
  if longitude_tujuan-longitude[0]==0: # sumbu x tetap / jalur vertikal (longitude tidak berubah)
    if latitude_tujuan-latitude[0]>0: # utara
      x = 1
    elif latitude_tujuan-latitude[0]<0: #selatan
      x = -1
    else:
      x = 0
    return x*(longitude_titik-longitude[0])

  else:
    if longitude_tujuan-longitude[0]>0: # jalur timur / timur laut / tenggara
      x = 1

    elif longitude_tujuan-longitude[0]<0: # jalur barat / barat laut / baratdaya
      x = -1

    a = (latitude_tujuan-latitude[0])/(longitude_tujuan-longitude[0]) #jarak titik ke garis(jalur)
    b = -1
    c = -a*longitude[0]+latitude[0]
    d = (a*longitude_titik+b*latitude_titik+c)/(math.sqrt((a**2)+(b**2)))
    return x*d

def hitung_error_jarak(latitude_tujuan, longitude_tujuan, latitude_titik, longitude_titik):
  d = math.fabs(math.sqrt((math.fabs(latitude_tujuan-latitude_titik))**2+(longitude_tujuan-longitude_titik)**2)) #jarak titik ke tujuan
  return d #mengembalikan nilai

mapscale = 18 #mengatur nilai mapscale
latitude = [] #mendeklarasikan list koordinat latitude sekarang
longitude = [] #mendeklarasikan list koordinat longitude sekarang

f = GPIO.PWM(12,100) #inisialisasi pin PWM
f.start(0) 
f.ChangeDutyCycle(90) #pengaturan nilai PWM

pwm = GPIO.PWM(11, 50) #inisialisasi pin PWM
pwm.start(0)
waypoint = 0

data_ke = 0

waypoint_tercapai = []

titik_tujuan = [
[-0.533475,117.123378],
[-0.533511,117.123306],
[-0.533619,117.123367]
]

while waypoint<len(titik_tujuan): #perulangan program ketika nilai waypoint kurang dari banyaknya jumlah titik tujuan

  port="/dev/serial0" #mendeklarasikan port serial UART
  ser=serial.Serial(port, baudrate=9600, timeout=0.5) #mengakses serial port dengan mengatur nilai baudrate dan timeout
  gpsdata=ser.readline() #mengembalikan list yang berisi baris – baris file dari awal sampai akhir pada variabel gpsdata
  # print("print 1 = ",gpsdata)
  
  try:
    gpsdata = gpsdata.decode("utf8") #proses komunikasi dengan memasukkan utf8 ke dalam variabel gpsdata
    # print("print 2 = ",gpsdata)

    try: #menguji suatu pernyataan
      gpsdata = gpsdata.split(',') #memisahkan antar data pada variabel gpsdata dengan koma

      if "GNRMC" in gpsdata[0]: #mengeksekusi kode jika kondisi True dan sebaliknya
        hrs, min, sec = gpsdata[1][0:2], gpsdata[1][2:4]. gpsdata[1][4:6] #memasukkan nilai gpsdata ke dalam format variabel hrs, min, secssssss
        day, month, year = gpsdata[9][0:2], gpsdata[9][2:4], gpsdata[9][4:6] #memasukkan indeks tertentu dari gpsdata ke dalam variabel day, month, year
        datetimeutc = "{}:{}:{} {}/{}/{}".format(hrs, min, sec, day, month, year) #memasukkan nilai jam, menit, detik, hari, bulan, tahun ke dalam format variabel datetimeutc
        datetimeutc = datetime.datetime.strptime(datetimeutc, '%H:%M:%S %d/%m/%y') #memasukkan nilai datetimeutc ke dalam variabel datetimeutc
        speed = round(float(gpsdata[7])*1.852,2) #perhitungan kecepatan
        message = "Datetime={} ,speed={} kmph".format(datetimeutc, speed) #memasukkan nilai datetimeutc dan speed ke dalam variabel message
        print(message) #menampilkan message

      if "GNGGA" in gpsdata[0]: #mengeksekusi kode jika kondisi True dan sebaliknya
        lat = dec2deg(float(gpsdata[2])) #memasukkan indeks ke 2 dari gps data ke dalam variabel lat dan mengonversinya ke dalam decimal degree
        lon = dec2deg(float(gpsdata[4])) #memasukkan indeks ke 2 dari gps data ke dalam variabel lon dan mengonversinya ke dalam decimal degree
        alt = gpsdata[9] #memasukkan indeks ke 9 dari gpsdata ke dalam variabel alt
        satcount = gpsdata[7] #memasukkan indeks ke 7 dari gpsdata ke dalam variabel satcount
        message = "Altitude={}, Satellites={}\n".format(alt, satcount) #memasukkan nilai alt dan satcount pada format dalam format variabel message
        gearth = "https://earth.google.com/web/search/@{},{},{}\n".format(lat,lon,alt) #memasukkan nilai lat, lon, dan alt ke dalam format link gearth
        mapsapp = "geo:{},{}\n".format(lat, lon) #memasukkan nilai lat dan lon ke dalam format variabel mapsapp
        map = "https://www.openstreetmap.org/#map={}/{}/{}\n\n".format(mapscale, lat, lon) #memasukkan nilai mapscale, lat, dan lon ke dalam link variabel map
        print(message, gearth, mapsapp, map) #menampilkan message, gearth, mapsapp, dan map

      # print("print 3 = ",gpsdata)
      # X = 1/0

    except: # menguji suatu pernyataan
      print("Masuk")
      # print(gpsdata)

      data_ke = data_ke+1
      print("data ke", data_ke)

      # ========================================================================== KONVERSI LAT/LONG DMS -> LAT/LONG DECIMAL DEGREE ==========================================================================
      data_latitude = float(gpsdata[3]) #memasukkan indeks ke 3 dari gpsdata ke variabel latitude (bentuk DMS)

      data_longitude = float(gpsdata[5]) #memasukkan indeks ke 5 dari gpsdata ke variabel longitude (bentuk DMS)

      if gpsdata[4]=="N" :
       dim_lat = 1
      elif gpsdata[4]=="S":
       dim_lat = -1

      xlat = float(dim_lat*((round(data_latitude,-2)/100)+((data_latitude - round(data_latitude,-2))/60))) #perhitungan nilai xlat (konversi latitude DMS ke latitude Decimal Degree)

      print()

      if gpsdata[6]=="E" :
       dim_long = 1
      elif gpsdata[6]=="W":
       dim_long = -1

      ylong = float(dim_long*((round(data_longitude,-2)/100)+((data_longitude - round(data_longitude,-2))/60))) #perhitungan nilai ylong (konversi longitude DMS ke Decimal Degree)

      # time = str(gpsdata[1]) #memasukkan indeks ke 1 dari gpsdata ke variabel waktu
      # date = gpsdata[9] #memasukkan indeks ke 9 dari gpsdata ke variabel tanggal

      # print("Latitude = ", xlat,'derajat\nLongitude  = ', ylong,'derajat') #menampilkan angka koordinat pada variabel xlat dan ylong
      
      latitude.append(xlat) #memasukkan nilai pada variabel xlat ke dalam list latitude sekarang
      longitude.append(ylong) #memasukkan nilai pada variabel xlat ke dalam list longitude sekaran

      # ====================================================================================================================================================

      if len(latitude)!=1 :
        latitude_tujuan = titik_tujuan[waypoint][0] 
        longitude_tujuan = titik_tujuan[waypoint][1]

        print("titik awal =  (",longitude[0],",",latitude[0],")")
        print("titik tujuan = (",longitude_tujuan,",",latitude_tujuan,")")
        print()

        # print("Long Lat terukur sebelumnya = (",longitude[-2],",",latitude[-2],")")
        print("Longitude latitude terukur sekarang = (",longitude[-1],",",latitude[-1],")")
        print()
        # ========================================================================== HITUNG GRADIENT JALUR & GERAK ==========================================================================
        
        try:
          g_vektor_jalur = ((latitude_tujuan-latitude[0])/(longitude_tujuan-longitude[0])) #perhitungan gradien jalur
        except:
          g_vektor_jalur = ("jalur vertikal")
        # print("Gradien jalur = ",g_vektor_jalur)
        
        try:
          g_vektor_gerak = ((latitude[-1]-latitude[-2])/(longitude[-1]-longitude[-2])) #perhitungan gradien gerak
        except:
          g_vektor_gerak = ("gerak vertikal")
        # print("Gradien gerak = ",g_vektor_gerak)

        # print()


        # ========================================================================== HITUNG ERROR JALUR ==========================================================================

        error_awal = hitung_error(latitude,longitude,latitude_tujuan,longitude_tujuan,latitude[-2],longitude[-2]) #menghitung eror awal

        # print("Error jalur awal =",error_awal," derajat")

        # if math.fabs(error_awal)<0.000005:
        #   print("di dalam jalur")
        # else:
        #  print("di luar jalur")

        error_akhir = hitung_error(latitude,longitude,latitude_tujuan,longitude_tujuan,latitude[-1],longitude[-1]) #menghitung eror akhir
        
        print("Error jalur =",error_akhir," derajat")

        # if math.fabs(error_akhir)<0.000005:
        #   print("di dalam jalur")
        # else:
        #   print("di luar jalur")

        # ========================================================================== HITUNG SELISIH ERROR JALUR ==========================================================================

        # print()

        selisih_error_jalur_sementara = error_awal - error_akhir

        # print(selisih_error_jalur_sementara)

        if error_awal<0 and error_akhir<0:
          dim_sel = -1
          # if selisih_error_jalur_sementara > 0:
          #   print("gerak menjauhi jalur")
          # elif selisih_error_jalur_sementara < 0:
          #   print("gerak mendekati jalur")
          # else:
          #   print("gerak sejajar dengan jalur")

        elif error_awal>0 and error_akhir>0:
          dim_sel = 1
          # if selisih_error_jalur_sementara < 0:
          #   print("gerak menjauhi jalur")
          # elif selisih_error_jalur_sementara > 0:
          #   print("gerak mendekati jalur")
          # else:
          #   print("gerak sejajar dengan jalur")

        elif error_awal==0 and error_akhir==0:
          dim_sel = 0
          # print("gerak sejajar dengan jalur")

        else:
          # print("gerak memotong jalur")
          if selisih_error_jalur_sementara>0:
            dim_sel = 1
            # print("memotong kanan ke kiri")
          elif selisih_error_jalur_sementara<0:
            dim_sel = -1
            # print("memotong kiri ke kanan")
          else:
            dim_sel = 0
            # print("gerak sejajar dengan jalur")


        selisih_error_jalur=dim_sel*selisih_error_jalur_sementara
        # print("selisih error jalur = "selisih_error_jalur)


        # ========================================================================== PERHITUNGAN SUDUT ==========================================================================

        # print()

        if g_vektor_jalur=="jalur vertikal" and g_vektor_gerak=="gerak vertikal":
          sudut = 0

        elif g_vektor_jalur!="jalur vertikal" and g_vektor_gerak!="gerak vertikal":

          if g_vektor_jalur*g_vektor_gerak!=-1: #tidak tegak lurus
            tan_a = (g_vektor_jalur-g_vektor_gerak)/(1+g_vektor_jalur*g_vektor_gerak) #perhitungan sudut tidak tegak lurus
            sudut_sementara = math.degrees(math.atan(tan_a))

            if error_awal > 0 and error_akhir > 0:
              # print("sebelah kanan jalur")

              if selisih_error_jalur < 0:
                # print("gerak menjauhi jalur")

                if sudut_sementara > 0:
                  sudut = sudut_sementara
                elif sudut_sementara < 0:
                  sudut = 180 + sudut_sementara
                else:
                  sudut = 0
                  
              elif selisih_error_jalur > 0:
                # print("gerak mendekati jalur")

                if sudut_sementara > 0:
                  sudut = sudut_sementara - 180
                elif sudut_sementara < 0:
                  sudut = sudut_sementara
                else:
                  sudut = 0
              
              else:
                sudut = 0
                  
            elif error_awal < 0 and error_akhir < 0:
              # print("sebelah kiri jalur")

              if selisih_error_jalur < 0:
                # print("gerak menjauhi jalur")

                if sudut_sementara > 0:
                  sudut = sudut_sementara - 180
                elif sudut_sementara < 0:
                  sudut = sudut_sementara
                else:
                  sudut = 0

              elif selisih_error_jalur > 0:
                # print("gerak mendekati jalur")

                if sudut_sementara > 0:
                  sudut = sudut_sementara
                elif sudut_sementara < 0:
                  sudut = 180 + sudut_sementara
                else:
                  sudut = 0
              
              else:
                sudut = 0

            elif error_awal < 0 and error_akhir > 0:
              # print("sudut on progress / 1")
              # sudut = 90
              if sudut_sementara > 0:
                sudut = sudut_sementara
              elif sudut_sementara < 0:
                sudut = 180 + sudut_sementara
              else:
                sudut = 0

            elif error_awal > 0 and error_akhir < 0:
              # print("sudut on progress / 2")
              # sudut = -90
              if sudut_sementara > 0:
                sudut = sudut_sementara - 180
              elif sudut_sementara < 0:
                sudut = sudut_sementara
              else:
                sudut = 0

            else:
              # print("sudut on progress / 3")
              sudut = 0

          else: #jika tegak lurus
            if selisih_error_jalur < 0:
              if error_akhir>0:
                sudut = 90
              else:
                sudut = -90

            elif selisih_error_jalur > 0:
              if error_akhir>0:
                sudut = -90
              else:
                sudut = 90

            else:
              sudut = 0

        elif g_vektor_jalur=="jalur vertikal": #gradien jalur vertikal, gradien gerak tidak vertikal
          if g_vektor_gerak>0: #perhitungan eror sudut dua garis
            tan_a = (0-g_vektor_gerak)/(1+0*g_vektor_gerak)
            sudut = math.degrees(math.atan(tan_a))+90 #menyesuaikan jika eror sudutnya sebelah kanan jalur

          elif g_vektor_gerak<0:
            tan_a = (0-g_vektor_gerak)/(1+0*g_vektor_gerak)
            sudut = math.degrees(math.atan(tan_a))-90 #menyesuaikan jika eror sudutnya sebelah kiri jalur
            
          else:
            if selisih_error_jalur < 0:
              if error_akhir>0:
                sudut = 90
              else:
                sudut = -90

            elif selisih_error_jalur > 0:
              if error_akhir>0:
                sudut = -90
              else:
                sudut = 90

            else:
              sudut = 0

        elif g_vektor_gerak=="gerak vertikal": #gradien gerak vertikal, gradien jalur tidak vertikal
          if g_vektor_jalur>0:
            tan_a = (g_vektor_jalur-0)/(1+g_vektor_jalur*0)
            sudut = math.degrees(math.atan(tan_a))-90 #menyesuaikan jika eror sudutnya sebelah kiri jalur
          elif g_vektor_jalur<0:
            tan_a = (g_vektor_jalur-0)/(1+g_vektor_jalur*0)
            sudut = math.degrees(math.atan(tan_a))+90 #menyesuaikan jika eror sudutnya sebelah kanan jalur
          else:
            if selisih_error_jalur < 0:
              if error_akhir>0:
                sudut = 90
              else:
                sudut = -90

            elif selisih_error_jalur > 0:
              if error_akhir>0:
                sudut = -90
              else:
                sudut = 90
            
            else:
              sudut = 0

        print("Error sudut =",sudut,"derajat") #jika eror positif maka mobil arah belok ke kanan dan sebaliknya

        print()



        # ========================================================================== HITUNG ERROR START/FINISH ==========================================================================

        g_vektor_start = -1/g_vektor_jalur
        
        if latitude[0]-latitude_tujuan==0:
          if longitude[0]-longitude_tujuan<0:
            x = 1
          if longitude[0]-longitude_tujuan>0:
            x = -1
          else:
            x = 0
          d1 = x*(longitude[0]-longitude[-1])
            
          # if d1>0 :
          #   print("di belakang garis start")
          # elif d1<0 :
          #   print("di depan garis start")
          # else:
          #   print("di garis start")

        elif latitude[0]-latitude_tujuan<0 :
          a = g_vektor_start
          b = -1
          c = -a*longitude[0]+latitude[0]
          d1 = (a*longitude[-1]+b*latitude[-1]+c)/(math.sqrt((a**2)+(b**2)))

          # if d1>0:
          #   print("di belakang garis start")
          # elif d1<0:
          #   print("di depan garis start")
          # else:
          #   print("di garis start")

        elif latitude[0]-latitude_tujuan>0 :
          a = g_vektor_start
          b = -1
          c = -a*longitude[0]+latitude[0]
          d1 = (a*longitude[-1]+b*latitude[-1]+c)/(math.sqrt((a**2)+(b**2)))

          # if d1>0:
          #   print("di depan garis start")
          # elif d1<0:
          #   print("di belakang garis start")
          # else:
          #   print("di garis start")

        # print(d1)

        # print()

        g_vektor_finish = g_vektor_start

        if latitude[0]-latitude_tujuan==0:
          if longitude[0]-longitude_tujuan<0:
            x2 = 1
          elif longitude[0]-longitude_tujuan>0:
            x2 = -1
          else:
            x2 = 0
          d2 = x2*(longitude_tujuan-longitude[-1])

          if d2>0 :
            asd = 0
            # print("di belakang garis finish")
          elif d2<0 :
            # print("di depan garis finish")
            if error_akhir > 0:
              setAngle(145)
              if sudut > 140:
                setAngle(115)
                time.sleep(5)

            elif error_akhir < 0:
              setAngle(80)
              if sudut < -140:
                setAngle(115)
                time.sleep(5)

            # else:
            #   print("di garis finish")

        elif latitude[0]-latitude_tujuan<0 :
          a = g_vektor_finish
          b = -1
          c = -a*longitude_tujuan+latitude_tujuan
          d2 = (a*longitude[-1]+b*latitude[-1]+c)/(math.sqrt((a**2)+(b**2)))

          if d2>0:
            asd = 0
            # print("di belakang garis finish")
          elif d2<0:
            # print("di depan garis finish")
            if error_akhir > 0:
              setAngle(145)
              if sudut > 140:
                setAngle(115)
                time.sleep(5)
          elif error_akhir < 0:
            setAngle(80)
            if sudut < -140:
              setAngle(115)
              time.sleep(5)

          # else:
          #   print("di garis finish")

        elif latitude[0]-latitude_tujuan>0 :
          a = g_vektor_finish
          b = -1
          c = -a*longitude_tujuan+latitude_tujuan
          d2 = (a*longitude[-1]+b*latitude[-1]+c)/(math.sqrt((a**2)+(b**2)))

          if d2>0:
            # print("di depan garis finish")
            if error_akhir > 0:
              setAngle(145)
              if sudut > 140:
                setAngle(115)
                time.sleep(5)
            elif error_akhir < 0:
              setAngle(80)
              if sudut < -140:
                setAngle(115)
                time.sleep(5)

          # elif d2<0:
          #   print("di belakang garis finish")
          
          # else:
          #   print("di garis finish")

        # print(d2)

        # print()

        # ========================================================================== HITUNG LONG LAT SEHARUSNYA ==========================================================================

        long_seharusnya = (g_vektor_jalur*longitude[0]-g_vektor_start*longitude[-1]+latitude[-1]-latitude[0])/(g_vektor_jalur-g_vektor_start)
        lat_seharusnya = g_vektor_start*(long_seharusnya-longitude[-1])+latitude[-1]
        print("Long Lat seharusnya = (",long_seharusnya,",",lat_seharusnya,")")

        # ========================================================================== HITUNG ERROR JARAK ==========================================================================

        error_jarak_awal = hitung_error_jarak(latitude_tujuan,longitude_tujuan,latitude[-2],longitude[-2]) #perhitungan eror jarak titik saat ini ke titik tujuan

        # print("Error jarak awal =", error_jarak_awal,"derajat")

        # if error_jarak_awal<0.000005:
        #   print("di dalam waypoint")
        # else:
        #   print("di luar waypoint")

        error_jarak_akhir = hitung_error_jarak(latitude_tujuan,longitude_tujuan,latitude[-1],longitude[-1]) #perhitungan eror jarak titik saat ini ke titik tujuan

        # print("Error jarak akhir =", error_jarak_akhir,"derajat")

        if error_jarak_akhir<0.00001:
          # print("di dalam waypoint")
          waypoint = waypoint + 1
          waypoint_tercapai.append(waypoint)
          latitude = [latitude_tujuan] #deklarasi ulang list latitude
          longitude =[longitude_tujuan] #deklarasi ulang list longitude
          print("waypoint",waypoint," tercapai")
          
        # else:
        #   print("di luar waypoint")


        # ========================================================================== IMPLEMENTASI SERVO ==========================================================================

        print()

        if error_akhir > 0.00001:
          if sudut >= 0 or -30 < sudut < 0:
            setAngle(80)
            b = "belok kiri"
          elif sudut <-60:
            setAngle(145)
            b = "belok kanan"
          else:
            setAngle(115)
            b = "lurus"
        elif -0.00001 <= error_akhir <= 0.00001:
          if sudut > 30:
            setAngle(80)
            b = "belok kiri"
          elif sudut < -30:
            setAngle(145)
            b = "belok kanan"
          else:
            setAngle(115)
            b = "lurus"
        elif error_akhir < -0.00001:
          if sudut <= 0 or 0 < sudut < 30:
            setAngle(145)
            b = "belok kanan"
          elif sudut >60:
            setAngle(80)
            b = "belok kiri"
          else:
            setAngle(115)
            b = "lurus"

        print(b)

      else :
        print("mulai bergerak")

      print()

      print("Waypoint tercapai = ",waypoint_tercapai)

      print()

      print("===============================")

  except:
    print("Keluar")
    print()

f.stop()
pwm.stop()
GPIO.cleanup()